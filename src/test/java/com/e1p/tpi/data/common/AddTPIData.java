package com.e1p.tpi.data.common;
import java.awt.AWTException;
import java.nio.file.*; 
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import com.automation.cucumber.core.FormData;

public class AddTPIData extends BaseData{

	private static final String CSV_PATH = "src/test/java/com/e1p/tpi/data/common/data_address.csv";

    private static final int addressIndex = 0;
    private static final int cityIndex = 1;
    private static final int stateIndex = 2;
    private static final int zipIndex = 3;
    
    private String name1 = "Smoke Testing" + " " + faker.idNumber().toString();
    private String name2 = "AMFAM Bank";
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phonenumber = faker.phoneNumber().phoneNumber();
    private String comments = "TEST";
    
    public AddTPIData() {
    }
	
  
	public void setRandomAddress() {
        helperRandomAddressFromList(readCSV(CSV_PATH));
    }
    
    private void helperRandomAddressFromList(ArrayList<ArrayList<String>> listAddresses) {
        ArrayList<String> generatedAddress = getRandomMatchingRow(listAddresses);
        setAddress(generatedAddress);
    }

    private void setAddress(ArrayList<String> generatedAddress) {
        this.address = generatedAddress.get(addressIndex);
        this.city = generatedAddress.get(cityIndex);
        this.state = generatedAddress.get(stateIndex);
        this.zip = generatedAddress.get(zipIndex);
    }

    // Reduce browser screen size to find locator correctly
    public void reduceBrowserScreenSize() throws AWTException
    {
    	 Robot robot = new Robot();
         int numberOfClick = 4; // Number of click 
           for (int i = 0; i < numberOfClick; i++) {
   			robot.keyPress(KeyEvent.VK_CONTROL);
   			robot.keyPress(KeyEvent.VK_SUBTRACT);
   			robot.keyRelease(KeyEvent.VK_SUBTRACT);
   			robot.keyRelease(KeyEvent.VK_CONTROL);
   			}
    }
    
	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	} 
    
    
    
    
}
