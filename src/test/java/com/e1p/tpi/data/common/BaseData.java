package com.e1p.tpi.data.common;

import com.automation.cucumber.core.FormData;
import com.automation.cucumber.core.utility.CSVReader;

import java.util.ArrayList;
import java.util.Random;

public class BaseData extends FormData {

    public BaseData() {}

    protected ArrayList<ArrayList<String>> readCSV(String path) {
        CSVReader reader = new CSVReader(path);
        Object[][] arRows = reader.readFile();

        ArrayList<ArrayList<String>> listRows = new ArrayList<>();

        for (Object[] row : arRows) {
            ArrayList<String> helperList = new ArrayList<>();
            for (int j = 0; j < arRows[0].length; j++) {
                helperList.add((String) row[j]);
            }
            listRows.add(helperList);
        }

        return listRows;
    }

    protected ArrayList<ArrayList<String>> getMatchingRows(ArrayList<ArrayList<String>> csvData,
                                                           int columnIndex, String searchValue) {

        ArrayList<ArrayList<String>> listMatchingRows = new ArrayList<>();

        for (ArrayList<String> row : csvData) {
            if (row.get(columnIndex).equals(searchValue)) {
                listMatchingRows.add(row);
            }
        }

        return listMatchingRows;
    }

    protected ArrayList<String> getRandomMatchingRow(ArrayList<ArrayList<String>> matchingRows) {
        return matchingRows.get(new Random().nextInt(matchingRows.size()));
    }
}
