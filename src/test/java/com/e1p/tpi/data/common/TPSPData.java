package com.e1p.tpi.data.common;

public class TPSPData extends BaseData {

	private String TPSPID = faker.idNumber().invalidSvSeSsn();
	private String EDITranslator = faker.idNumber().validSvSeSsn();
	private String TINInternalID = "000000000";
	private String PrimaryName = faker.funnyName().name();

	public TPSPData() {

	}

	public String getTPSPID() {
		return TPSPID;
	}

	public void setTPSPID(String tPSPID) {
		TPSPID = tPSPID;
	}

	public String getEDITranslator() {
		return EDITranslator;
	}

	public void setEDITranslator(String eDITranslator) {
		EDITranslator = eDITranslator;
	}

	public String getTINInternalID() {
		return TINInternalID;
	}

	public void setTINInternalID(String tINInternalID) {
		TINInternalID = tINInternalID;
	}

	public String getPrimaryName() {
		return PrimaryName;
	}

	public void setPrimaryName(String primaryName) {
		PrimaryName = primaryName;
	}
}
