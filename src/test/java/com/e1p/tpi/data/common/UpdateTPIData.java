package com.e1p.tpi.data.common;

import java.util.ArrayList;

public class UpdateTPIData extends BaseData {
	private static final String CSV_PATH = "src/test/java/com/e1p/tpi/data/common/TPINumber.csv";

	private static final int TPINumberIndex = 0;

	private String TPINumber;
	private String name2 = "AMFAM Bank";
	private String name1 = "Bank";
	private String phonenumber = faker.phoneNumber().phoneNumber();

	public UpdateTPIData() {

	}

	public void setRandomTPINumber() {
		helperRandomTPINumberFromList(readCSV(CSV_PATH));
	}

	private void helperRandomTPINumberFromList(ArrayList<ArrayList<String>> listTPINumbers) {
		ArrayList<String> generatedTPINumber = getRandomMatchingRow(listTPINumbers);
		setTPINumber(generatedTPINumber);
	}

	private void setTPINumber(ArrayList<String> generatedTPINumber) {
		this.TPINumber = generatedTPINumber.get(TPINumberIndex);
	}

	// get set methods
	public String getTPINumber() {
		return TPINumber;
	}

	public void setTPINumber(String tPINumber) {
		TPINumber = tPINumber;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

}
