package com.e1p.tpi.data.common;

import com.automation.cucumber.core.FormData;

public class UserLogin extends FormData {

	public String username;
	public String password;

	public UserLogin() {
		username= System.getProperty("username");
	
	    password= System.getProperty("password");

	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
