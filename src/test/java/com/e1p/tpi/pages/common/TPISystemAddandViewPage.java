package com.e1p.tpi.pages.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.selenium.cucumber.core.BasePage;
import com.e1p.tpi.data.common.AddTPIData;
import com.e1p.tpi.data.common.UserLogin;

public class TPISystemAddandViewPage extends BasePage {
	int waitDuration = 35;
	int waitDuration2 = 40;

	// ##########################Home ADD screen Objects###########################
	@FindBy(xpath = "//*[@id='idp-discovery-username']")
	public WebElement inputUsername;

	@FindBy(xpath = "//*[@id='idp-discovery-submit']")
	public WebElement NextStart;

	@FindBy(xpath = "//*[@id='okta-signin-submit']")
	public WebElement NextB;

	@FindBy(xpath = "//*[@id='input9']")
	public WebElement inputPassword;

	@FindBy(xpath = "//*[@id='form7']/div[2]/input")
	public WebElement btnVerfiy;

	//@FindBy(xpath = "/html/body/app-root/div/mat-drawer-container/mat-drawer/div/div/div/div[2]/span")
	//public String title;

	@FindBy(xpath = "//*[contains(text(), 'TPI Module')]")
	public WebElement tpimodule;

	@FindBy(xpath = "//*[contains(text(), 'Home Add')]")
	public WebElement tpihomeadd;

	@FindBy(xpath = "//input[contains(@name, 'mailing-name-1')]")
	public WebElement Name1;

	@FindBy(xpath = "//input[contains(@name, 'mailing-name-2')]")
	public WebElement Name2;

	@FindBy(xpath = "//input[contains(@name, 'address-line-1')]")
	public WebElement Address;

	@FindBy(xpath = "//input[contains(@name, 'state')]")
	public WebElement State;
	
	@FindBy(css = "input[formcontrolname= 'zip4']")
	private WebElement zip4;

	@FindBy(xpath = "//input[contains(@name, 'zip5')]")
	public WebElement Zip;
	@FindBy(xpath = "//input[contains(@name, 'city')]")
	public WebElement City;

	@FindBy(xpath = "//input[contains(@name, 'telephone')]")
	public WebElement Phone;

	@FindBy(css = "mat-expansion-panel-header[id='mat-expansion-panel-header-2']")
	public WebElement CommentSrnclk;

	@FindBy(css = "textarea[formcontrolname='comment'] ")
	public WebElement Comment;

	@FindBy(css = "body > app-root > div > mat-drawer-container > mat-drawer-content > app-tpi-home > div > mat-card > form > div > button:nth-child(1)")
	public WebElement Savebtn;

	@FindBy(xpath = "//*[@id='mat-radio-8']/label/div[1]/div[2]")
	public WebElement PreferedRbtn;

	@FindBy(css = "#mat-radio-11 > label > div.mat-radio-container > div.mat-radio-outer-circle")
	public WebElement PolicyNEbtn;

	@FindBy(xpath = " //*[contains(text(), 'TPI Home Added Successfully')]")
	public WebElement Successfulmsg;

	@FindBy(xpath = "//*[contains(text(), 'TPI Number of Inserted record:')]")
	public WebElement AddTPINo;

	@FindBy(xpath = "//button[contains(text(), 'OK')]")
	public WebElement okbtn;

	@FindBy(css = "tr[class='mat-row ng-star-inserted']")
	public WebElement rows;

	// ##########################Home Search screen Objects########################################

	@FindBy(xpath = "//*[contains(text(), 'Home Search')]")
	public WebElement HomeSrch;

	@FindBy(xpath = "//input[contains(@name, 'tpi-number')]")
	public WebElement TPIIN;

	@FindBy(xpath = "//*[contains(@class, 'mat-icon notranslate mat-menu-trigger mat-icon-no-color')]")
	public WebElement Details;

	@FindBy(xpath = "//*[contains(@class, 'btn btn-view')]")
	public WebElement View;

	@FindBy(xpath = "//button[contains(text(), 'Search ')]")
	public WebElement Searchbtn;

	// #############################################################################################

	public TPISystemTPSPpage loginSuccessfully(UserLogin data) {
		login(data);
		return new TPISystemTPSPpage(getDriver());
	}

	// Login in TPI System
	public void login(UserLogin data) {
		inputUsername.sendKeys(data.getUsername());
		NextStart.click();
		NextB.click();
		inputPassword.sendKeys(data.getPassword());
		btnVerfiy.click();
	}

	// Get Page Title of Tpi System
	public String click_on_login_button_and_verfiy_title() {
		waitForClickable(tpimodule);
		return getDriver().getTitle();
		
		 
	}

	// Click on HomeAddscreen
	public void User_clicks_HomeAddscrn() {
		waitForClickable(tpimodule);
		click(tpimodule);
		click(tpihomeadd);
	}

	// User Enters mandatory fields data
	public void User_Provides_Mandatory_Data() throws InterruptedException {
		
		AddTPIData tpiAddress = new AddTPIData();
		tpiAddress.setRandomAddress();

		// Set values in UI
				
		// Wait for visible Name 1 field and Enter data
		populateTextField(Name1, tpiAddress.getName1());

		// Wait for visible Name 2 field and Enter data
		populateTextField(Name2, tpiAddress.getName2());
		
		// Wait for visible Address field and Enter data
		populateTextField(Address,tpiAddress.getAddress());
			
		// Wait for visible City field and Enter data
		populateTextField(City, tpiAddress.getAddress());
		
		// Wait for visible State field and Enter data
		populateTextField(State,tpiAddress.getState());

		// Wait for visible Zip5 field and Enter data
		populateTextField(Zip, tpiAddress.getZip());
		
	   // wait for load the ZIP4 field value
		changeWait(waitDuration2);
			       
		// Provide Phone Number in Phone Field
		populateTextField(Phone, tpiAddress.getPhonenumber());
		              
		// Preferred field is selected
		jsClick(PreferedRbtn);

		// Policy Notice field is selected
		jsClick(PolicyNEbtn);
		
		resetWait();
		// Provide some text in Comment Field
		jsClick(CommentSrnclk);
		// Enter text in comment field
		waitForClickable(Comment);
		populateTextField(Comment, tpiAddress.getComments());
		
	}

	public void click_on_save_button() {

		if (Savebtn.isEnabled()) {
			waitForClickable(Savebtn);
			jsClick(Savebtn);
			
		} else {
			System.out.println("User is not able to click Save Button");
			//getDriver().close();
		}

	}

	public String user_gets_confirm_message() {
		return Successfulmsg.getText();

	}

	public void search_new_added_tpi(String TPIN) {
		if (HomeSrch.isEnabled()) {
			jsClick(HomeSrch);
		} else {
			System.out.println("User is not able to search a TPI");
			//getDriver().close();

		}
		
		// View a new TPI is added or not by providing generate new TPI number
		waitForClickable(TPIIN);
		populateTextField(TPIIN, TPIN);
		
		// Click on search button for View the TPI
		waitForClickable(Searchbtn);
		jsClick(Searchbtn);
		
		// To view the added TPI information in home screen
		jsClick(Details);
		jsClick(View);
		changeWait(waitDuration);

	}

	public TPISystemAddandViewPage(WebDriver driver) {
		super(driver);
	}

	
}
