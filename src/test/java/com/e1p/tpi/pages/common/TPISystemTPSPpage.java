package com.e1p.tpi.pages.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.selenium.cucumber.core.BasePage;
import com.e1p.tpi.data.common.TPSPData;
import com.e1p.tpi.data.common.UserLogin;

public class TPISystemTPSPpage extends BasePage{
	
	public TPISystemTPSPpage(WebDriver driver) {
		super(driver);
		
	}
	TPISystemAddandViewPage page= new TPISystemAddandViewPage(getDriver());
	TPSPData data= new TPSPData();
	
//################################################Page Object##################################	
	
	@FindBy(xpath="//*[contains(text() ,'TPSP Module')]")
	public WebElement TPSPModule;
	
	@FindBy(xpath="//*[contains(text() ,'Home Add')]")
	public WebElement homeAddTpsp;
	
	@FindBy(css="input[formcontrolname= 'tpspId']")
	public WebElement TPSPID;
	
	@FindBy(css="input[formcontrolname= 'ediTranslatorId']")
	public WebElement ediTranslatorId;
	
	@FindBy(css="input[formcontrolname= 'tinInternalId']")
	public WebElement tinInternalId;
	
	@FindBy(css="input[formcontrolname= 'namePrimary']")
	public WebElement namePrimary;
	
	@FindBy(css="#mat-radio-41 > label > div.mat-radio-container > div.mat-radio-inner-circle")
	public WebElement Billingradiobtn;
	
	@FindBy(css="div>mat-select[formcontrolname= 'plDueDay1']")
	public WebElement billdate;
		
	@FindBy(css="div[class='cdk-overlay-pane']")
	public WebElement dropdwnvalues;
	
	@FindBy(xpath="//span[text()= '10 - 26']")
	public WebElement dropdwnvalue;
	
	
	@FindBy(css="body > app-root > div > mat-drawer-container > mat-drawer-content > app-tpsp-home > div > mat-card > div > button:nth-child(1)")
	public WebElement savebtn;
	
	@FindBy(css="h2[class='swal2-title']")
	public WebElement TPSPADDmsg;
	
	
	@FindBy(css="div[class='swal2-html-container']")
	public WebElement TPSPmsgno;
	
	@FindBy(css="button[class='swal2-confirm swal2-styled']")
	public WebElement TPSPmsgokbtn;
	
	
	//###################################################HOME SEARCH OBJ######################################
	@FindBy(xpath="//span[text()='Home Search']")
	public WebElement Homesearch;
	
	@FindBy(css="input[formcontrolname='tpspId']")
	public WebElement tpspIDsearch;
	
	@FindBy(css="button[class='btn btn-primary']")
	public WebElement tpspsearch;
	
	@FindBy(css="tr[class='mat-row ng-star-inserted']")
	public WebElement TPSPResponerow;
	
	@FindBy(css="mat-icon[class='mat-icon notranslate mat-menu-trigger mat-icon-no-color']")
	public WebElement Details;
	
	@FindBy(xpath = "//*[contains(@class, 'btn btn-view')]")
	public WebElement View;
	
	
//#################################################################################################################	
	
	public TPISystemTPSPpage loginSuccessfully(UserLogin data) {
		 login(data);
		return new TPISystemTPSPpage(getDriver());
	}

	public void login(UserLogin data) {
		page.inputUsername.sendKeys(data.getUsername());
		page.NextStart.click();
		page.NextB.click();
		page.inputPassword.sendKeys(data.getPassword());
		page.btnVerfiy.click();
			
	}
    

    public String verfiy_page_title() {
    	waitForClickable(TPSPModule);
    	return getDriver().getTitle();
    }


    public void user_navigate_to_tpsp_module_home_add() {
    	 //Click on TPSP Module
    	waitForClickable(TPSPModule);
    	click(TPSPModule);
    	 //Click on HomeAddTPSP Module
    	click(homeAddTpsp);
    }


    public void user_enters_mandatory_field_in_tpsp_addscreen() {
        // Provide data in TPSP field 
    	populateTextField(TPSPID, data.getTPSPID());
    	 // Provide data in ediTranslator field 
    	populateTextField(ediTranslatorId, data.getEDITranslator());
    	 // Provide data in tin field
    	populateTextField(tinInternalId, data.getTINInternalID());
    	// Provide data in nameprimary field
    	populateTextField(namePrimary, data.getPrimaryName());
    	
    	// click on Billing radio button
    	waitForClickable(Billingradiobtn);
    	jsClick(Billingradiobtn);
    	waitForClickable(billdate);
    	jsClick(billdate);
    	waitForClickable(dropdwnvalue);
    	jsClick(dropdwnvalue);
    	jsClick(savebtn);
    	
    }

    public String user_gets_confirm_message() {
    	return TPSPADDmsg.getText();
    	
    }

   public void user_verfiy__added_tpsp_information(String TPSPIDno){
    	
    	if (Homesearch.isEnabled()) {
			jsClick(Homesearch);
		} else {
			System.out.println("User is not able to search a TPI");
			//getDriver().close();

		}
		
		// View a new TPI is added or not by providing generate new TPI number
		populateTextField(tpspIDsearch,TPSPIDno );
		//tpspIDsearch.sendKeys(TPSPIDno);
		jsClick(tpspsearch);
		 
    }
   
   public void user_view_tpsp_information_and_close_the_browser() {
	     jsClick(Details);
		 
		 jsClick(View);
		
   }

}
