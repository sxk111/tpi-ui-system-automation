package com.e1p.tpi.pages.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.selenium.cucumber.core.BasePage;
import com.e1p.tpi.data.common.AddTPIData;
import com.e1p.tpi.data.common.UpdateTPIData;
import com.e1p.tpi.data.common.UserLogin;

public class TPISystemUpdateandViewPage extends TPISystemAddandViewPage {

	public TPISystemUpdateandViewPage(WebDriver driver) {
		super(driver);
	}

	UpdateTPIData tpin = new UpdateTPIData();
	int waitDuration = 35; // 2 sec duration wait time after send keys value

	@FindBy(xpath = "//*[contains(text(), 'TPI Module')]")
	public WebElement tpimodule;

	@FindBy(css = "button[class='btn btn-edit']")
	private WebElement Edit;

	@FindBy(css = "mat-expansion-panel-header[id='mat-expansion-panel-header-1']")
	private WebElement TPSPScrn;

	@FindBy(css = "mat-expansion-panel-header[id='mat-expansion-panel-header-4']")
	private WebElement TPSPScrnView;

	@FindBy(css = "input[formcontrolname='bill_tpsp_id']")
	private WebElement TPSPID;

	@FindBy(css = "input[formcontrolname='bn_tpsp_name']")
	private WebElement TPSPname;

	@FindBy(css = "input[formcontrolname='bill_ref_id']")
	private WebElement Branchcde;

	@FindBy(css = "#mat-radio-14 > label > div.mat-radio-container > div.mat-radio-outer-circle")
	private WebElement AFIClassic;

	@FindBy(css = "#mat-radio-17 > label > div.mat-radio-container > div.mat-radio-outer-circle")
	private WebElement AFIGuidewire;

	@FindBy(css = "button[class='btn btn-primary']")
	private WebElement updatebtn;

	@FindBy(css = "div[class='swal2-html-container']")
	private WebElement updateMsg;

	@FindBy(css = "input[name='tin-number']")
	private WebElement TINssn;

	@FindBy(css = "button[class='swal2-confirm swal2-styled']")
	private WebElement Updatemsgokbtn;

	@FindBy(css = "tr[class='mat-row ng-star-inserted']")
	public WebElement rows;

	@FindBy(css = "div[class='swal2-html-container']")
	public WebElement errormsg;

	@FindBy(css = "button[class='swal2-confirm swal2-styled']")
	public WebElement errormsgokbtn;

	@FindBy(xpath = "//*[contains(@class, 'mat-icon notranslate mat-menu-trigger mat-icon-no-color')]")
	public WebElement Details;

	@FindBy(xpath = "//*[contains(@class, 'btn btn-view')]")
	public WebElement View;

	@FindBy(xpath = "//button[contains(text(), 'Search ')]")
	public WebElement Searchbtn;

	// ###############################################################################################################

	public TPISystemTPSPpage loginSuccessfully(UserLogin data) {
		login(data);
		return new TPISystemTPSPpage(getDriver());
	}

	
	public void login(UserLogin data) {

		inputUsername.sendKeys(data.getUsername());
		NextStart.click();
		NextB.click();
		inputPassword.sendKeys(data.getPassword());
		btnVerfiy.click();
	}

	public String verfiy_title() {		
		waitForClickable(tpimodule);
		return getDriver().getTitle();
	}

	// clicking the search button
	public void user_clicks_homesearchscrn() {
		waitForClickable(tpimodule);
		// Click on TPI Module
		jsClick(tpimodule);
		// Click on home search
		jsClick(HomeSrch);
	}

	public void user_update_additional_information() {
		tpin.setRandomTPINumber();

		// To type text In TPINumber Field.
		populateTextField(TPIIN, tpin.getTPINumber());
		
		// Click on search button for View the TPI
		waitForClickable(Searchbtn);
		jsClick(Searchbtn);
		
		// To view the added TPI information in home screen
		jsClick(Details);
		jsClick(Edit);
		// Clear value if exist and set new value
		populateTextField(Phone,tpin.getPhonenumber());
		
		// Clear value if exist and set new value
		populateTextField(Name2, tpin.getName2());
		
		// Click on Update button
		waitForClickable(updatebtn);
		jsClick(updatebtn);

	}

	public String verfiy_tpi_updated_msg() {
		return updateMsg.getText();
	}

	public void verfiy_updated_name2_field_value() {
		// click on Update Message window
		waitForClickable(Updatemsgokbtn);
		Updatemsgokbtn.click();

		TPIIN.sendKeys(tpin.getTPINumber());
		// Click on search button for View the TPI

		jsClick(Searchbtn);
		changeWait(waitDuration);
	}

	public void user_view_the_updated_tpi_information_in_home_search_module() {
		// To view the updated TPI information in home screen
		jsClick(Details);
		jsClick(View);

		changeWait(waitDuration);
	}

}
