package com.e1p.tpi.runner;

import org.testng.annotations.DataProvider;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"pretty", "json:target/cucumber-parallel/output.json",
        "com.automation.cucumber.reporter.ZephyrReporter"},
        monochrome = true,
        strict = true,
       features = {"target/zephyr/features"},
         //  features = {"src/test/resources/features/Add.feature"},
        glue = {"com.e1p.tpi.steps"})
public class CucumberRunnerIT extends AbstractTestNGCucumberTests {
    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }

}
