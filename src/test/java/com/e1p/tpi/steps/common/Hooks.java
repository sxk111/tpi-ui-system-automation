package com.e1p.tpi.steps.common;

import java.util.concurrent.TimeUnit;

import com.automation.selenium.cucumber.core.BaseWebStep;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;


public class Hooks extends BaseWebStep {
	
	@Before
	public void beforeScenario(Scenario scenario) {
		initializeWebDriver(scenario);
//		getDriver().navigate().to("https://dev-pe.amfam2dev.guidewire.net/ProducerEngage/dist/html/index-agents.html#!/auth/login");
	}
	
	@After
	public void afterScenario(Scenario scenario) {
		killWebDriver(scenario);
	}

}
