package com.e1p.tpi.steps.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import com.automation.selenium.cucumber.core.BaseWebStep;
import com.e1p.tpi.data.common.AddTPIData;
import com.e1p.tpi.data.common.UserLogin;
import com.e1p.tpi.pages.common.TPISystemAddandViewPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TPISystemADDandViewSteps extends BaseWebStep {

	// public WebDriver driver;
	// public TPISystemLogin tpi;
	TPISystemAddandViewPage page = new TPISystemAddandViewPage(getDriver());
	AddTPIData tpiData = new AddTPIData();
	String newTPIID;

	@Given("I navigate to {string}")
	public void navigateToHomePage(String url) {
		getDriver().navigate().to(url);
	}

	@When("User enters Email as {string} and Password as {string}")
	public void login(String username, String password) {
		setData(username, password);
		page.loginSuccessfully(getData());
	}

	
    @And("Click on Login and verfiy Page title")
	public void click_on_login_button_and_verfiy_title() {
		Assert.assertEquals(page.click_on_login_button_and_verfiy_title(), "DPML");
	}

	@And("When user login with vaild credentials, click on Home Add for add a TPI")
	public void User_clicks_HomeAddscrn() throws AWTException, InterruptedException {
		page.User_clicks_HomeAddscrn();
		tpiData.reduceBrowserScreenSize();
	}

	@And("User provides address as Name1, Address1, City, State, Zip5, Name2, Comment, PREFERRED, POLICY NOTICE ONLY, Phone with all mandatory and optional fields")
	public void User_Provides_Mandotory_Data() throws InterruptedException {
		page.User_Provides_Mandatory_Data();
	}

	@And("User click on save button to add a TPI")
	public void click_on_save_button() {
		page.click_on_save_button();
	}

	@Then("User gets confirm message as TPI Home Added Successfully!!")
	public void User_Gets_Confirm_Message() throws IOException {
		page.user_gets_confirm_message();
		String title = page.user_gets_confirm_message();
		Assert.assertEquals(title, "TPI Home Added Successfully");
		// Get Generate TPI ID Number
		String TPINo = page.AddTPINo.getText();

		// we are using java string split method to get only TPI Number
		String[] splitString = TPINo.split(":");	
		//Click on Confirm Message button
		page.okbtn.click();
		// System.out.println(splitString[1]);
		newTPIID = splitString[1];
		
	}

	@Then("User view the Added TPI with generate TPI ID number in Home search module")
	public void search_new_added_tpi() {
		page.search_new_added_tpi(newTPIID);
		//getDriver().quit();
	}

	public void setData(String username, String password) {
		UserLogin page = new UserLogin();
		page.setUsername(username);
		page.setPassword(password);
	}

	public UserLogin getData() {
		return getData(UserLogin.class);
	}

}
