package com.e1p.tpi.steps.common;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import com.automation.selenium.cucumber.core.BaseWebStep;
import com.e1p.tpi.data.common.AddTPIData;
import com.e1p.tpi.data.common.UserLogin;
import com.e1p.tpi.pages.common.TPISystemTPSPpage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TPISystemTPSPSteps extends BaseWebStep {

	TPISystemTPSPpage tpsppage = new TPISystemTPSPpage(getDriver());
	AddTPIData tpiData = new AddTPIData();
	String newTPSPID;

	@Given("User navigate To TPIsystem as {string}")
	public void UsernavigateTohomePageas2(String url) {
		getDriver().navigate().to(url);
	}

	@When("User enters credentials as {string} and {string}")
	public void login(String username, String password) {
		setData(username, password);
		tpsppage.loginSuccessfully(getData());
	}

	@And("Click on Login and verfiy Page title should be DPML")
	public void verfiy_page_title() {
		Assert.assertEquals(tpsppage.verfiy_page_title(), "DPML");
	}

	@And("User Navigate to TPSP module - Home ADD")
	public void user_navigate_to_tpsp_module_home_add() throws AWTException {
		tpsppage.user_navigate_to_tpsp_module_home_add();
		tpiData.reduceBrowserScreenSize();

	}

	@And("User enters mandatory field in TPSP Add screen and click on save button")
	public void user_enters_mandatory_field_in_tpsp_addscreen() {
		tpsppage.user_enters_mandatory_field_in_tpsp_addscreen();
	}

	@And("User gets confirm message as TPSP Home Added Successfully!!")
	public void user_gets_confirm_message_as_TPSP_Home_Added_Successfully() {
		tpsppage.user_gets_confirm_message();
		String Msg = tpsppage.user_gets_confirm_message();
		Assert.assertEquals(Msg, "TPSP Home Added Successfully!!");

		String TPINo = tpsppage.TPSPmsgno.getText();
		// System.out.println(TPINo);
		tpsppage.TPSPmsgokbtn.click();
		// we are using java string split method to get only TPI Number
		String[] splitString = TPINo.split("is ");
		String[] splitString1 = splitString[1].split(" !");
		newTPSPID = splitString1[0];
	}

	@And("User vaildate new TPSP is created in TPSP module")
	public void user_verfiy__added_tpsp_information() {
		tpsppage.user_verfiy__added_tpsp_information(newTPSPID);
		String response = tpsppage.TPSPResponerow.getText();
		System.out.println(response);

		boolean conditionVerify = false;
		if (response.contains(newTPSPID)) {
			conditionVerify = true;
		}
		if (conditionVerify == true) {
			System.out.println("PASSED- TPSP ID has created Successfully");

		} else {
			System.out.println("FAILED- TPSP ID has not created Successfully");
		}
	}

	@Then("User view TPSP information and close the browser")
	public void user_view_tpsp_information_and_close_the_browser() {
		tpsppage.user_view_tpsp_information_and_close_the_browser();
		//getDriver().quit();
	}

	public void setData(String username, String password) {
		UserLogin page = new UserLogin();
		page.setUsername(username);
		page.setPassword(password);
	}

	public UserLogin getData() {
		return getData(UserLogin.class);
	}

}
