package com.e1p.tpi.steps.common;

import java.awt.AWTException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import com.automation.selenium.cucumber.core.BasePage;
import com.automation.selenium.cucumber.core.BaseWebStep;
import com.e1p.tpi.data.common.AddTPIData;
import com.e1p.tpi.data.common.UpdateTPIData;
import com.e1p.tpi.data.common.UserLogin;
import com.e1p.tpi.pages.common.TPISystemAddandViewPage;
import com.e1p.tpi.pages.common.TPISystemUpdateandViewPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TPISystemUpdateandViewSteps extends BaseWebStep {

	TPISystemUpdateandViewPage Upage = new TPISystemUpdateandViewPage(getDriver());
	AddTPIData tpiData = new AddTPIData();
	UpdateTPIData tpiUpdateData = new UpdateTPIData();
	
	String name2 = "AMFAM BANK";

	// Open the Chrome Browser and URL
	@Given("User navigate To HomePage as {string}")
	public void user_navigate_To_HomePage_as1(String url) throws InterruptedException {
		getDriver().navigate().to(url);
		  
	}

	@When("User enters credentials Email as {string} and Password as {string}")
	public void login(String username, String password) {
		setData(username, password);
		Upage.loginSuccessfully(getData());
	}

	// clicking the search button
	@And("User Successfully Login and Page title should be DPML")
	public void verfiy_title() {
		Assert.assertEquals(Upage.click_on_login_button_and_verfiy_title(), "DPML");
	}

	@And("When user login with vaild credentials, click on Home Search for update a TPI")
	public void user_clicks_homesearchscrn() throws AWTException {
		Upage.user_clicks_homesearchscrn();
		tpiData.reduceBrowserScreenSize();
		
	}

	@And("User search a tpi for update additional information as Name2, Phone and click on update button")
	public void user_update_additional_information() {
		// Reading TPInumber ID from text file written earlier during Add TPI scenario
		Upage.user_update_additional_information();
	}

	@And("User gets confirm message as TPI Home Updated Successfully!!")
	public void verfiy_tpi_updated_msg() {
		Upage.verfiy_tpi_updated_msg();
		String Msg = Upage.verfiy_tpi_updated_msg();
		Assert.assertEquals(Msg, "TPI Home Updated Successfully");
	}

	@And("Verfiy Updated Name2 field value")
	public void verfiy_updated_name2_field_value() throws IOException {
		Upage.verfiy_updated_name2_field_value();
		String response = Upage.rows.getText();
		System.out.println(response);

		boolean conditionVerify = false;

		if (response.contains(name2)) {
			conditionVerify = true;
		}

		if (conditionVerify == true) {
			System.out.println("PASSED- TPI has Updated the data Successfully");

		} else {
			System.out.println("FAILED- TPI has not Updated the data Successfully");
		}
	}

	@Then("User view the Updated TPI information in Home search module")
	public void user_view_the_updated_tpi_information_in_home_search_module() {
		Upage.user_view_the_updated_tpi_information_in_home_search_module();
		//getDriver().quit();
	}

	public void setData(String username, String password) {
		UserLogin page = new UserLogin();
		page.setUsername(username);
		page.setPassword(password);
	}

	public UserLogin getData() {
		return getData(UserLogin.class);
	}

}
