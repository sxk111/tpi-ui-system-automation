Feature: Add
 Background:
 
  Scenario: Add a TPI in TPI Management UI
    Given I navigate to "https://amfamtpiqa1.pragmaedge.com:8181/"
    When  User enters Email as "username" and Password as "password"
    And   Click on Login and verfiy Page title
    And   When user login with vaild credentials, click on Home Add for add a TPI
    And   User provides address as Name1, Address1, City, State, Zip5, Name2, Comment, PREFERRED, POLICY NOTICE ONLY, Phone with all mandatory and optional fields
    And   User click on save button to add a TPI
    And   User gets confirm message as TPI Home Added Successfully!!
    Then  User view the Added TPI with generate TPI ID number in Home search module
    
    
    
  