Feature: TPSP
 Background:

 
  Scenario: Add a TPSP in TPI Management UI
    Given User navigate To TPIsystem as "https://amfamtpiqa1.pragmaedge.com:8181/"
    When  User enters credentials as "username" and "password"
    And   Click on Login and verfiy Page title should be DPML
    And   User Navigate to TPSP module - Home ADD
    And   User enters mandatory field in TPSP Add screen and click on save button
    And   User gets confirm message as TPSP Home Added Successfully!!
    And   User vaildate new TPSP is created in TPSP module
    Then  User view TPSP information and close the browser