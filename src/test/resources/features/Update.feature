
Feature: Update
 Background:

  Scenario: Update a TPI in TPI Management UI
    Given User navigate To HomePage as "https://amfamtpiqa1.pragmaedge.com:8181/"
    When  User enters credentials Email as "username" and Password as "password"
    And   User Successfully Login and Page title should be DPML
    And   When user login with vaild credentials, click on Home Search for update a TPI
    And   User search a tpi for update additional information as Name2, Phone and click on update button
    And   User gets confirm message as TPI Home Updated Successfully!!
    And   Verfiy Updated Name2 field value
    Then  User view the Updated TPI information in Home search module